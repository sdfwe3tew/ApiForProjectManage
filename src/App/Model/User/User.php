<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/22
 * Time: 10:01
 */

namespace App\Model\User;
use App\Common\CommonModel;
use App\Domain\Team\Team;

class User extends CommonModel
{

    public function getUserFullById($user_id)
    {
        $prefix = \PhalApi\DI()->config->get('dbs.tables.__default__.prefix');
        $sql = 'SELECT *,u.id as u_user_id,u.id as id,l.eng_name as l_eng_name,p.eng_name as p_eng_name '
            . 'FROM ' . $prefix . 'user AS u '
            . 'LEFT JOIN ' . $prefix . 'team_user AS tu '
            . ' ON tu.user_id = u.id '
            . 'LEFT JOIN ' . $prefix . 'company_team AS t '
            . ' ON tu.team_id = t.id '
            . 'JOIN ' . $prefix . 'user_level AS l '
            . 'ON u.level_id = l.id '
            . 'JOIN ' . $prefix . 'user_position AS p '
            . 'ON u.position_id = p.id '
            . 'AND u.id = :id ';
        $params = array(':id' => $user_id);
        $info = \PhalApi\DI()->notorm->notTable->queryRows($sql, $params);
        $domain_team = new Team();
        if ($info) {
            $team_path = $domain_team->getTeamPath($info[0]['team_id'],'－');
            $info[0]['team_path'] = $team_path;
            return $info[0];
        }else{
            return $info;
        }

    }

    protected function getTableName($id)
    {
        return 'user';
    }

}