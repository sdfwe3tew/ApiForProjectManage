### 项目管理系统接口
### 演示地址
> [https://project.vilson.xyz](https://project.vilson.xyz)
### 项目说明
> 项目管理系统的后台接口，基于[Phalapi](https://www.phalapi.net/)
### 部署步骤
1. 将*data*目录下的sql文件导入数据库
2. 修改*config*目录下的 dbs文件，连接数据库
3. 运行composer安装依赖
4. 运行 域名/项目名/public，出现成功页面，部署成功
### 界面截图
![1](https://static.vilson.xyz/1.png)
![1](https://static.vilson.xyz/2.png)
![1](https://static.vilson.xyz/3.png)
![1](https://static.vilson.xyz/4.png)
![1](https://static.vilson.xyz/5.png)
![1](https://static.vilson.xyz/6.png)
![1](https://static.vilson.xyz/7.png)

